import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:path_provider/path_provider.dart';

import 'package:to_do_list/features/login/presentation/pages/splash_screen.dart';

import 'package:to_do_list/core/blocs/bloc_exports.dart';
import 'core/services/app_router.dart';
import 'firebase_options.dart';
// import 'assets/theme/app_theme.dart';
import 'assets/theme/theme.dart';

void main() async {
  await GetStorage.init();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  HydratedBloc.storage = await HydratedStorage.build(
      storageDirectory: await getApplicationDocumentsDirectory());

  runApp(MyApp(
    appRouter: AppRouter(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key, required this.appRouter}) : super(key: key);
  final AppRouter appRouter;
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => TasksBloc()),
        BlocProvider(create: (context) => SwitchBloc()),
        BlocProvider(create: (context) => ShowPopUpBloc()),
      ],
      child: BlocBuilder<SwitchBloc, SwitchState>(
        builder: (context, state) {
          return OverlaySupport.global(
            child: MaterialApp(
              debugShowCheckedModeBanner: false,
              title: 'Flutter Tasks App',
              theme: state.switchValue
                  ? AppTheme.darkTheme()
                  : AppTheme.lightTheme(),
              // ? AppThemes.appThemeData[AppTheme.darkTheme]
              // : AppThemes.appThemeData[AppTheme.lightTheme],
              // home: const TabsScreen(),
              onGenerateRoute: appRouter.onGenerateRoute,
              home: const SplashScreen(),
            ),
          );
        },
      ),
    );
  }
}
