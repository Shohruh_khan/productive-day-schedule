import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:to_do_list/core/blocs/bloc_exports.dart';
import 'package:to_do_list/core/widgets/w_scale.dart';

import '../../../../assets/colors/colors.dart';
import '../../../../core/models/formz/formz_type.dart';
import '../../../../core/widgets/w_button.dart';
import '../../../../core/widgets/w_field/w_text_field.dart';
import '../../../tasks/presentation/pages/tabs_screen.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({super.key});
  static const id = 'register_screen';

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _emailController = TextEditingController();

  final TextEditingController _passwordController = TextEditingController();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        'Register',
        style: Theme.of(context)
            .textTheme
            .headline2!
            .copyWith(fontSize: 18, fontWeight: FontWeight.w600),
      )),
      body: ListView(
        children: [
          const SizedBox(height: 56),
          WTextField(
            title: 'Email',
            textStyle: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(fontSize: 16, fontWeight: FontWeight.w600),
            titleTextStyle: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(fontSize: 16, fontWeight: FontWeight.w500),
            margin: const EdgeInsets.symmetric(horizontal: 16),
            hintText: 'Enter Email',
            hintTextStyle: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(fontSize: 14, fontWeight: FontWeight.w400),
            controller: _emailController,
            onChanged: (value) {},
            validationType: FormzType.email,
          ),
          const SizedBox(height: 12),
          WTextField(
            title: 'Password',
            textStyle: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(fontSize: 16, fontWeight: FontWeight.w600),
            titleTextStyle: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(fontSize: 16, fontWeight: FontWeight.w500),
            margin: const EdgeInsets.symmetric(horizontal: 16),
            hintText: 'Enter Password',
            hintTextStyle: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(fontSize: 14, fontWeight: FontWeight.w400),
            controller: _passwordController,
            onChanged: (value) {},
            validationType: FormzType.password,
          ),
          const SizedBox(height: 16),
          Align(
            child: BlocBuilder<SwitchBloc, SwitchState>(
              builder: (context, state) {
                return WButton(
                  color: state.switchValue ? darkTextColor : primary,
                  margin: const EdgeInsets.all(16),
                  onTap: () {
                    // Get username and password from the user.Pass the data to
// helper method
                    _auth
                        .createUserWithEmailAndPassword(
                            email: _emailController.text,
                            password: _passwordController.text)
                        .then((value) {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const TabsScreen()));
                    }).onError((error, stackTrace) {
                      context.read<ShowPopUpBloc>().add(
                          ShowPopUpEvent.showFailure(text: error.toString()));
                    });
                  },
                  child: const Text('Register'),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
