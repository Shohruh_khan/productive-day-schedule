import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:to_do_list/features/login/presentation/pages/login_screen.dart';

import '../../../tasks/presentation/pages/tabs_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final GetStorage _getStorage = GetStorage();
  @override
  void initState() {
    openNextPage(context);
    // TODO: implement initState
    super.initState();
  }

  openNextPage(BuildContext context) {
    print('my authorisation token is:  ${_getStorage.read('token')}');
    Timer(const Duration(milliseconds: 200), () {
      if (_getStorage.read('token') == null ||
          _getStorage.read('token') == '') {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => const LoginScreen()));
      } else {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => const TabsScreen()));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
