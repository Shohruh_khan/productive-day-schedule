import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_storage/get_storage.dart';
import 'package:to_do_list/core/models/formz/formz_type.dart';
import 'package:to_do_list/core/widgets/w_field/w_text_field.dart';
import 'package:to_do_list/core/widgets/w_scale.dart';
import 'package:to_do_list/features/login/presentation/pages/register_screen.dart';

import 'package:to_do_list/core/blocs/bloc_exports.dart';

import '../../../../assets/colors/colors.dart';
import '../../../../core/pages/w_scaffold.dart';
import '../../../../core/widgets/w_button.dart';
import '../../../tasks/presentation/pages/tabs_screen.dart';
import 'forgot_password_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);
  static const id = 'login_screen';

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WScaffold(
      appBar: AppBar(
        title: Text(
          'Login',
          style: Theme.of(context)
              .textTheme
              .headline2!
              .copyWith(fontSize: 18, fontWeight: FontWeight.w600),
        ),
      ),
      body: ListView(
        children: [
          const SizedBox(height: 56),
          WTextField(
            title: 'Email',
            textStyle: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(fontSize: 16, fontWeight: FontWeight.w600),
            titleTextStyle: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(fontSize: 16, fontWeight: FontWeight.w500),
            margin: const EdgeInsets.symmetric(horizontal: 16),
            hintText: 'Enter Email',
            hintTextStyle: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(fontSize: 14, fontWeight: FontWeight.w400),
            controller: _emailController,
            onChanged: (value) {},
            validationType: FormzType.email,
          ),
          const SizedBox(height: 12),
          WTextField(
            title: 'Password',
            textStyle: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(fontSize: 16, fontWeight: FontWeight.w600),
            titleTextStyle: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(fontSize: 16, fontWeight: FontWeight.w500),
            margin: const EdgeInsets.symmetric(horizontal: 16),
            hintText: 'Enter Password',
            hintTextStyle: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(fontSize: 14, fontWeight: FontWeight.w400),
            controller: _passwordController,
            onChanged: (value) {},
            validationType: FormzType.password,
          ),
          BlocBuilder<SwitchBloc, SwitchState>(
            builder: (context, state) {
              return WButton(
                color: state.switchValue ? darkTextColor : primary,
                margin: const EdgeInsets.all(16),
                onTap: () {
                  _auth
                      .signInWithEmailAndPassword(
                          email: _emailController.text,
                          password: _passwordController.text)
                      .then((value) => {
                            GetStorage().write('token', value.user!.uid),
                            GetStorage().write('email', value.user!.email),
                            print(
                                'user id from Storage: ${GetStorage().read('token')}'),
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const TabsScreen()))
                          })
                      .onError((error, stackTrace) => {
                            context.read<ShowPopUpBloc>().add(
                                  ShowPopUpEvent.showFailure(
                                      text: 'Wrong email or password'),
                                ),
                          });
                },
                child: Text(
                  'Login',
                  style: Theme.of(context)
                      .textTheme
                      .headline2!
                      .copyWith(fontSize: 16, fontWeight: FontWeight.w600),
                ),
              );
            },
          ),
          Align(
            child: WScaleAnimation(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const RegisterScreen()));
              },
              child: Text(
                'Don\'t have an Account?',
                style: Theme.of(context)
                    .textTheme
                    .caption!
                    .copyWith(fontSize: 16, fontWeight: FontWeight.w500),
              ),
            ),
          ),
          const SizedBox(height: 16),
          Align(
            child: WScaleAnimation(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const ForgotPasswordScreen()));
              },
              child: Text(
                'Forget Password',
                style: Theme.of(context).textTheme.caption!.copyWith(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
              ),
            ),
          ),
          BlocBuilder<SwitchBloc, SwitchState>(
            builder: (context, state) {
              return Switch(
                activeColor: lightBlue,
                value: state.switchValue,
                onChanged: (newValue) {
                  newValue
                      ? context.read<SwitchBloc>().add(SwitchOnEvent())
                      : context.read<SwitchBloc>().add(SwitchOffEvent());
                },
              );
            },
          )
        ],
      ),
    );
  }
}
