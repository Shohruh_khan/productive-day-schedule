import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:to_do_list/features/login/presentation/pages/login_screen.dart';

import '../../../../assets/colors/colors.dart';
import '../../../../core/blocs/show_pop_up/show_pop_up_bloc.dart';
import '../../../../core/blocs/switch/switch_bloc.dart';
import '../../../../core/models/formz/formz_type.dart';
import '../../../../core/widgets/w_button.dart';
import '../../../../core/widgets/w_field/w_text_field.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);
  static const id = 'forgot_password_screen';

  @override
  State<ForgotPasswordScreen> createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Forgot Password',
          style: Theme.of(context)
              .textTheme
              .headline2!
              .copyWith(fontSize: 18, fontWeight: FontWeight.w600),
        ),
      ),
      body: ListView(
        children: [
          const SizedBox(height: 56),
          WTextField(
            title: 'Email',
            textStyle: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(fontSize: 16, fontWeight: FontWeight.w600),
            titleTextStyle: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(fontSize: 16, fontWeight: FontWeight.w500),
            margin: const EdgeInsets.symmetric(horizontal: 16),
            hintText: 'Enter Email',
            hintTextStyle: Theme.of(context)
                .textTheme
                .headline2!
                .copyWith(fontSize: 14, fontWeight: FontWeight.w400),
            controller: _emailController,
            onChanged: (value) {},
            validationType: FormzType.email,
          ),
          BlocBuilder<SwitchBloc, SwitchState>(
            builder: (context, state) {
              return WButton(
                color: state.switchValue ? darkTextColor : primary,
                margin: const EdgeInsets.all(16),
                onTap: () async {
                  await _auth
                      .sendPasswordResetEmail(
                          email: _emailController.text.trim())
                      .then((value) {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => const LoginScreen()));
                  }).onError((error, stackTrace) {
                    context.read<ShowPopUpBloc>().add(
                        ShowPopUpEvent.showFailure(text: 'Email is incorrect'));
                  });
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Icon(Icons.mail_outline),
                    SizedBox(width: 10),
                    Text("Reset Password")
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
