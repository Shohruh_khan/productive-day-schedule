// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'validation_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ValidationEvent {
  String get value => throw _privateConstructorUsedError;
  VoidCallback get onSuccess => throw _privateConstructorUsedError;
  ValueChanged<String> get onFailure => throw _privateConstructorUsedError;
  FormzType get type => throw _privateConstructorUsedError;
  Rules? get rules => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String value, VoidCallback onSuccess,
            ValueChanged<String> onFailure, FormzType type, Rules? rules)
        validate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String value, VoidCallback onSuccess,
            ValueChanged<String> onFailure, FormzType type, Rules? rules)?
        validate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String value, VoidCallback onSuccess,
            ValueChanged<String> onFailure, FormzType type, Rules? rules)?
        validate,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_FieldValidated value) validate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_FieldValidated value)? validate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_FieldValidated value)? validate,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ValidationEventCopyWith<ValidationEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ValidationEventCopyWith<$Res> {
  factory $ValidationEventCopyWith(
          ValidationEvent value, $Res Function(ValidationEvent) then) =
      _$ValidationEventCopyWithImpl<$Res, ValidationEvent>;
  @useResult
  $Res call(
      {String value,
      VoidCallback onSuccess,
      ValueChanged<String> onFailure,
      FormzType type,
      Rules? rules});
}

/// @nodoc
class _$ValidationEventCopyWithImpl<$Res, $Val extends ValidationEvent>
    implements $ValidationEventCopyWith<$Res> {
  _$ValidationEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? value = null,
    Object? onSuccess = null,
    Object? onFailure = null,
    Object? type = null,
    Object? rules = freezed,
  }) {
    return _then(_value.copyWith(
      value: null == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String,
      onSuccess: null == onSuccess
          ? _value.onSuccess
          : onSuccess // ignore: cast_nullable_to_non_nullable
              as VoidCallback,
      onFailure: null == onFailure
          ? _value.onFailure
          : onFailure // ignore: cast_nullable_to_non_nullable
              as ValueChanged<String>,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as FormzType,
      rules: freezed == rules
          ? _value.rules
          : rules // ignore: cast_nullable_to_non_nullable
              as Rules?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_FieldValidatedCopyWith<$Res>
    implements $ValidationEventCopyWith<$Res> {
  factory _$$_FieldValidatedCopyWith(
          _$_FieldValidated value, $Res Function(_$_FieldValidated) then) =
      __$$_FieldValidatedCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String value,
      VoidCallback onSuccess,
      ValueChanged<String> onFailure,
      FormzType type,
      Rules? rules});
}

/// @nodoc
class __$$_FieldValidatedCopyWithImpl<$Res>
    extends _$ValidationEventCopyWithImpl<$Res, _$_FieldValidated>
    implements _$$_FieldValidatedCopyWith<$Res> {
  __$$_FieldValidatedCopyWithImpl(
      _$_FieldValidated _value, $Res Function(_$_FieldValidated) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? value = null,
    Object? onSuccess = null,
    Object? onFailure = null,
    Object? type = null,
    Object? rules = freezed,
  }) {
    return _then(_$_FieldValidated(
      null == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String,
      onSuccess: null == onSuccess
          ? _value.onSuccess
          : onSuccess // ignore: cast_nullable_to_non_nullable
              as VoidCallback,
      onFailure: null == onFailure
          ? _value.onFailure
          : onFailure // ignore: cast_nullable_to_non_nullable
              as ValueChanged<String>,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as FormzType,
      rules: freezed == rules
          ? _value.rules
          : rules // ignore: cast_nullable_to_non_nullable
              as Rules?,
    ));
  }
}

/// @nodoc

class _$_FieldValidated
    with DiagnosticableTreeMixin
    implements _FieldValidated {
  const _$_FieldValidated(this.value,
      {required this.onSuccess,
      required this.onFailure,
      required this.type,
      this.rules});

  @override
  final String value;
  @override
  final VoidCallback onSuccess;
  @override
  final ValueChanged<String> onFailure;
  @override
  final FormzType type;
  @override
  final Rules? rules;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ValidationEvent.validate(value: $value, onSuccess: $onSuccess, onFailure: $onFailure, type: $type, rules: $rules)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ValidationEvent.validate'))
      ..add(DiagnosticsProperty('value', value))
      ..add(DiagnosticsProperty('onSuccess', onSuccess))
      ..add(DiagnosticsProperty('onFailure', onFailure))
      ..add(DiagnosticsProperty('type', type))
      ..add(DiagnosticsProperty('rules', rules));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FieldValidated &&
            (identical(other.value, value) || other.value == value) &&
            (identical(other.onSuccess, onSuccess) ||
                other.onSuccess == onSuccess) &&
            (identical(other.onFailure, onFailure) ||
                other.onFailure == onFailure) &&
            (identical(other.type, type) || other.type == type) &&
            (identical(other.rules, rules) || other.rules == rules));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, value, onSuccess, onFailure, type, rules);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_FieldValidatedCopyWith<_$_FieldValidated> get copyWith =>
      __$$_FieldValidatedCopyWithImpl<_$_FieldValidated>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String value, VoidCallback onSuccess,
            ValueChanged<String> onFailure, FormzType type, Rules? rules)
        validate,
  }) {
    return validate(value, onSuccess, onFailure, type, rules);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String value, VoidCallback onSuccess,
            ValueChanged<String> onFailure, FormzType type, Rules? rules)?
        validate,
  }) {
    return validate?.call(value, onSuccess, onFailure, type, rules);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String value, VoidCallback onSuccess,
            ValueChanged<String> onFailure, FormzType type, Rules? rules)?
        validate,
    required TResult orElse(),
  }) {
    if (validate != null) {
      return validate(value, onSuccess, onFailure, type, rules);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_FieldValidated value) validate,
  }) {
    return validate(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_FieldValidated value)? validate,
  }) {
    return validate?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_FieldValidated value)? validate,
    required TResult orElse(),
  }) {
    if (validate != null) {
      return validate(this);
    }
    return orElse();
  }
}

abstract class _FieldValidated implements ValidationEvent {
  const factory _FieldValidated(final String value,
      {required final VoidCallback onSuccess,
      required final ValueChanged<String> onFailure,
      required final FormzType type,
      final Rules? rules}) = _$_FieldValidated;

  @override
  String get value;
  @override
  VoidCallback get onSuccess;
  @override
  ValueChanged<String> get onFailure;
  @override
  FormzType get type;
  @override
  Rules? get rules;
  @override
  @JsonKey(ignore: true)
  _$$_FieldValidatedCopyWith<_$_FieldValidated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ValidationState {
  FormzStatus get status => throw _privateConstructorUsedError;
  String get value => throw _privateConstructorUsedError;
  FormzType get formzType => throw _privateConstructorUsedError;
  String get validationMessage => throw _privateConstructorUsedError;
  Rules? get rules => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ValidationStateCopyWith<ValidationState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ValidationStateCopyWith<$Res> {
  factory $ValidationStateCopyWith(
          ValidationState value, $Res Function(ValidationState) then) =
      _$ValidationStateCopyWithImpl<$Res, ValidationState>;
  @useResult
  $Res call(
      {FormzStatus status,
      String value,
      FormzType formzType,
      String validationMessage,
      Rules? rules});
}

/// @nodoc
class _$ValidationStateCopyWithImpl<$Res, $Val extends ValidationState>
    implements $ValidationStateCopyWith<$Res> {
  _$ValidationStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? value = null,
    Object? formzType = null,
    Object? validationMessage = null,
    Object? rules = freezed,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      value: null == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String,
      formzType: null == formzType
          ? _value.formzType
          : formzType // ignore: cast_nullable_to_non_nullable
              as FormzType,
      validationMessage: null == validationMessage
          ? _value.validationMessage
          : validationMessage // ignore: cast_nullable_to_non_nullable
              as String,
      rules: freezed == rules
          ? _value.rules
          : rules // ignore: cast_nullable_to_non_nullable
              as Rules?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ValidationStateCopyWith<$Res>
    implements $ValidationStateCopyWith<$Res> {
  factory _$$_ValidationStateCopyWith(
          _$_ValidationState value, $Res Function(_$_ValidationState) then) =
      __$$_ValidationStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {FormzStatus status,
      String value,
      FormzType formzType,
      String validationMessage,
      Rules? rules});
}

/// @nodoc
class __$$_ValidationStateCopyWithImpl<$Res>
    extends _$ValidationStateCopyWithImpl<$Res, _$_ValidationState>
    implements _$$_ValidationStateCopyWith<$Res> {
  __$$_ValidationStateCopyWithImpl(
      _$_ValidationState _value, $Res Function(_$_ValidationState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? value = null,
    Object? formzType = null,
    Object? validationMessage = null,
    Object? rules = freezed,
  }) {
    return _then(_$_ValidationState(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      value: null == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String,
      formzType: null == formzType
          ? _value.formzType
          : formzType // ignore: cast_nullable_to_non_nullable
              as FormzType,
      validationMessage: null == validationMessage
          ? _value.validationMessage
          : validationMessage // ignore: cast_nullable_to_non_nullable
              as String,
      rules: freezed == rules
          ? _value.rules
          : rules // ignore: cast_nullable_to_non_nullable
              as Rules?,
    ));
  }
}

/// @nodoc

class _$_ValidationState
    with DiagnosticableTreeMixin
    implements _ValidationState {
  const _$_ValidationState(
      {this.status = FormzStatus.pure,
      this.value = '',
      this.formzType = FormzType.none,
      this.validationMessage = '',
      this.rules = null});

  @override
  @JsonKey()
  final FormzStatus status;
  @override
  @JsonKey()
  final String value;
  @override
  @JsonKey()
  final FormzType formzType;
  @override
  @JsonKey()
  final String validationMessage;
  @override
  @JsonKey()
  final Rules? rules;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ValidationState(status: $status, value: $value, formzType: $formzType, validationMessage: $validationMessage, rules: $rules)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ValidationState'))
      ..add(DiagnosticsProperty('status', status))
      ..add(DiagnosticsProperty('value', value))
      ..add(DiagnosticsProperty('formzType', formzType))
      ..add(DiagnosticsProperty('validationMessage', validationMessage))
      ..add(DiagnosticsProperty('rules', rules));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ValidationState &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.value, value) || other.value == value) &&
            (identical(other.formzType, formzType) ||
                other.formzType == formzType) &&
            (identical(other.validationMessage, validationMessage) ||
                other.validationMessage == validationMessage) &&
            (identical(other.rules, rules) || other.rules == rules));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, status, value, formzType, validationMessage, rules);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ValidationStateCopyWith<_$_ValidationState> get copyWith =>
      __$$_ValidationStateCopyWithImpl<_$_ValidationState>(this, _$identity);
}

abstract class _ValidationState implements ValidationState {
  const factory _ValidationState(
      {final FormzStatus status,
      final String value,
      final FormzType formzType,
      final String validationMessage,
      final Rules? rules}) = _$_ValidationState;

  @override
  FormzStatus get status;
  @override
  String get value;
  @override
  FormzType get formzType;
  @override
  String get validationMessage;
  @override
  Rules? get rules;
  @override
  @JsonKey(ignore: true)
  _$$_ValidationStateCopyWith<_$_ValidationState> get copyWith =>
      throw _privateConstructorUsedError;
}
