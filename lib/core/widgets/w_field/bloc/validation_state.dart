part of 'validation_bloc.dart';

@Freezed()
class ValidationState with _$ValidationState {
  const factory ValidationState({
    @Default(FormzStatus.pure) FormzStatus status,
    @Default('') String value,
    @Default(FormzType.none) FormzType formzType,
    @Default('') String validationMessage,
    @Default(null) Rules? rules,
  }) = _ValidationState;
}
