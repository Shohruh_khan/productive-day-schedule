import 'package:flutter/material.dart';
import 'package:to_do_list/assets/colors/colors.dart';

class WDivider extends StatelessWidget {
  final double height;
  final bool isVertical;
  final double width;
  final Color color;
  final EdgeInsets margin;
  const WDivider({
    super.key,
    this.height = 1,
    this.color = dividerColor,
    this.width = 1,
    this.margin = EdgeInsets.zero,
    this.isVertical = false,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      width: isVertical ? width : double.infinity,
      margin: margin,
      height: isVertical ? double.infinity : height,
      color: color,
    );
  }
}
