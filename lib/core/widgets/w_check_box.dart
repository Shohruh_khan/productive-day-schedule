import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:to_do_list/assets/icons/icons.dart';

class WCheckBox extends StatefulWidget {
  // final ValueChanged<bool> onChange;
  final double width;
  final double height;
  final bool isChecked;
  final bool isDisabled;
  final double? radius;
  final Color? enabledBackgroundColor;
  final Color? disabledBackgroundColor;
  final Color? checkedColor;

  const WCheckBox({
    Key? key,
    // required this.onChange,
    required this.isChecked,
    this.width = 20,
    this.height = 20,
    this.isDisabled = false,
    this.radius,
    this.enabledBackgroundColor,
    this.disabledBackgroundColor,
    this.checkedColor,
  }) : super(key: key);

  @override
  State<WCheckBox> createState() => _WCheckBoxState();
}

class _WCheckBoxState extends State<WCheckBox> {
  late bool isChecked;
  @override
  void initState() {
    isChecked = widget.isChecked;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      width: widget.width,
      height: widget.height,
      padding: const EdgeInsets.all(4),
      decoration: BoxDecoration(
        color: widget.isChecked
            ? widget.enabledBackgroundColor ??
                Theme.of(context).colorScheme.primary
            : Colors.transparent,
        borderRadius: BorderRadius.circular(widget.radius ?? 6),
        border: Border.all(
          width: 1,
          color: widget.isChecked
              ? widget.enabledBackgroundColor ??
                  Theme.of(context).colorScheme.primary
              : widget.disabledBackgroundColor ??
                  Theme.of(context).colorScheme.onPrimary,
        ),
      ),
      duration: const Duration(milliseconds: 250),
      child: AnimatedOpacity(
        opacity: widget.isChecked ? 1 : 0,
        duration: const Duration(milliseconds: 250),
        child: SvgPicture.asset(
          AppIcons.transferActive,
          color: widget.checkedColor,
        ),
      ),
    );
  }
}
