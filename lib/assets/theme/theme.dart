import 'package:flutter/material.dart';

import '../colors/colors.dart';

class AppTheme {
  static ThemeData lightTheme() => ThemeData(
        fontFamily: 'Barlow',
        brightness: Brightness.light,
        scaffoldBackgroundColor: background,
        floatingActionButtonTheme: const FloatingActionButtonThemeData(
          backgroundColor: primary,
          foregroundColor: Colors.white,
        ),
        textTheme: const TextTheme(
          headline1: headline2,
          headline2: headline1,
          headline3: headline3,
          headline4: headline4,
          headline5: headline5,
          headline6: headline6,
          caption: caption,
        ),
        colorScheme: const ColorScheme(
          brightness: Brightness.light,
          primary: primary,
          onPrimary: white,
          secondary: lightBrown,
          onSecondary: accentGrey,
          error: red,
          onError: arrowGrey,
          background: lightBrown,
          onBackground: white,
          surface: lightBrown,
          onSurface: black,
        ),
      );
  static ThemeData darkTheme() => ThemeData(
        fontFamily: 'Barlow',
        brightness: Brightness.dark,
        //tabBarTheme: const TabBarTheme(labelColor: red),
        // appBarTheme:
        //     const AppBarTheme(backgroundColor: scaffoldBackgroundColor),
        scaffoldBackgroundColor: scaffoldBackgroundColor,
        // floatingActionButtonTheme: const FloatingActionButtonThemeData(
        //   backgroundColor: Color(0XFFD3D3D3),
        //   // foregroundColor: white,
        // ),
        textTheme: const TextTheme(
          headline1: headline1,
          headline2: headline2,
          headline3: headline3,
          headline4: headline4,
          headline5: headline5,
          headline6: headline6,
          caption: caption,
        ),
        colorScheme: const ColorScheme(
          brightness: Brightness.dark,
          // primary: blue,
          primary: lightBlue,
          onPrimary: grey,
          // secondary: blue,
          secondary: lightBlue,
          onSecondary: accentGrey,
          error: red,
          onError: arrowGrey,
          background: scaffoldBackgroundColor,
          onBackground: white,
          surface: darkTextColor,
          onSurface: white,
        ),
      );
  // Fonts
  static const headline1 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w700,
    color: dark,
  );
  static const headline2 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w700,
    color: white,
  );
  static const headline3 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w700,
    color: black,
  );
  static const headline4 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w600,
    color: grey,
  );
  static const headline5 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w500,
    color: lightGrey,
  );
  static const headline6 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w600,
    color: red,
  );

  static const caption = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w600,
    color: lightBlue,
  );
}
