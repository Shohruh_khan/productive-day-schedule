import 'package:flutter/cupertino.dart';

const white = _white;
const background = _whiteAccent;
const scaffoldBackgroundColor = _c131524;
const stroke = _solitude;
const secondary = _heather;
const grey = _c494646;
const dark = _prussianBlue;
const red = _cD64751;
const black = _black;
const blue = _cornflowerBlue;
const greyBlue = _echoBlue;
const darkTextColor = _licorice;
const greyTextColor = _c989DAD;

const lightGrey = _c5C5C5C;
const lightBlue = _c84C4F4;
const lightGreen = _c90EE90;
const green = _green;

const brown = _cBF7C2E;
const primary = _cF5BC41;
const accentGrey = _c1C2E45;
const lightBrown = _cDECDB8;
const arrowGrey = _c606060;
const dividerColor = _cBDBDBD;

const expensesContainerBackgroundColor1 = _midnight_express;
const expensesContainerBackgroundColor2 = _linkWater;
const verticalDividerColor = _nobel;

const categoriesColor1 = _darkOrange;
const categoriesColor2 = _summerSky;
const categoriesColor3 = _mediumOrchid;

const transactionContainerColor1 = _radicalRed;
const transactionContainerColor2 = _fruitSalad;

const transactionContainerText = _darkGrey;

const transactionCostColor1 = _radicalRed;
const transactionCostColor2 = _limeGreen;

const cardInfoColor1 = _perano;
const cardInfoColor2 = _mediumPurple;
const cardInfoColor3 = _slateBlue;

// Link: http://www.color-blindness.com/color-name-hue/

const _white = Color(0XFFFFFFFF);
const _whiteAccent = Color(0XFFFAFAFA);
const _solitude = Color(0XFFECEDF0);
const _heather = Color(0XFFAFB6C4);
const _prussianBlue = Color(0XFF001444);
const _black = Color(0XFF000000);
const _echoBlue = Color(0xFFA9BDCB);
const _cornflowerBlue = Color(0XFF0535DC);
const _licorice = Color(0XFF2C3246);
const _green = Color(0XFF00FF00);

const _darkGrey = Color(0XFFABABAB);
const _radicalRed = Color(0XFFFF2752);
const _midnight_express = Color(0XFF17192B);
const _linkWater = Color(0XFFBFC8D2);
const _nobel = Color(0XFF9B9B9B);
const _limeGreen = Color(0XFF44DB4A);
const _darkOrange = Color(0XFFFF8700);
const _summerSky = Color(0XFF32A7E2);
const _mediumOrchid = Color(0XFFB548C6);
const _fruitSalad = Color(0XFF4DAA52);

const _perano = Color(0xFFC7C1EF);
const _mediumPurple = Color(0xFF968BE2);
const _slateBlue = Color(0xFF5B4AD1);

// Unnamed colors
const _c131524 = Color(0XFF131524);
const _c989DAD = Color(0XFF989DAD);
const _cBDBDBD = Color(0XFFBDBDBD);
const _c84C4F4 = Color(0XFF84C4F4);
const _c494646 = Color(0XFF494646);
const _c5C5C5C = Color(0XFF5C5C5C);
const _cD64751 = Color(0XFFD64751);
const _cF5BC41 = Color(0XFFF5BC41);
const _cBF7C2E = Color(0XFFBF7C2E);
const _c1C2E45 = Color(0XFF1C2E45);
const _cDECDB8 = Color(0XFFDECDB8);
const _c606060 = Color(0XFF606060);
const _c90EE90 = Color(0XFF90EE90);
