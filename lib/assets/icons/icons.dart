abstract class AppIcons {
  AppIcons._();

  /////////////////   Global Icons   ////////////////////////////

  static const homeActive = 'assets/icons/global/home.svg';
  static const homeInactive = 'assets/icons/global/home_inactive.svg';

  static const transferActive = 'assets/icons/global/transfer_active.svg';
  static const transferInactive = 'assets/icons/global/transfer_inactive.svg';

  static const serviceActive = 'assets/icons/global/service_active.svg';
  static const serviceInactive = 'assets/icons/global/service_inactive.svg';
  static const success = 'assets/icons/global/success.svg';
  static const error = 'assets/icons/global/validation_error.svg';
  static const close = 'assets/icons/global/close.svg';
  static const eye = 'assets/icons/global/eye.svg';
  static const validationError = 'assets/icons/global/validation_error.svg';
  static const back = 'assets/icons/global/back.svg';
  static const add = 'assets/icons/global/add.svg';
  static const search = 'assets/icons/global/search_text_field.svg';
  static const clearTextField = 'assets/icons/global/close_text_field.svg';
  /////////////////   Global Icons End   ////////////////////////

  /////////////////   Main Icons   ////////////////////////////
  static const reminder = 'assets/icons/main/reminder.svg';
  static const up = 'assets/icons/main/up.svg';
  static const down = 'assets/icons/main/down.svg';
  static const health = 'assets/icons/main/health.svg';
  static const shopping = 'assets/icons/main/shopping.svg';
  static const transport = 'assets/icons/main/transport.svg';
  static const cancel = 'assets/icons/main/cancel.svg';

  /////////////////   Main Icons End   ////////////////////////////

}
